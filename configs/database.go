package configs

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"os"

	_ "github.com/jinzhu/gorm/dialects/mysql"

)

func InitDB() *gorm.DB {
	//open a db connection
	var err error

	dbSource := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

	db, err := gorm.Open(os.Getenv("DB_TYPE"), dbSource)
	db.LogMode(true)

	if err != nil {
		panic("failed to connect database")
	}

	return db
}
