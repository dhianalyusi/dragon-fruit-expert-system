# Dragon Fruit Diseases Identification

Export System for Dragon Fruit Diseases Identification using Forward Chaining method and Breadth First Search

## Requirement

- Go Lang (https://github.com/golang/go)
- Viper (https://github.com/spf13/viper)
- Graph (https://github.com/soniakeys/graph)

## Installations

Clone Project

```
# Clone Project
$ git clone https://gitlab.com/dragon-fruit-expert-system.git
```

Install Dependencies

```
# Install Dependencies
$ dep ensure
```

## Run migration DB

```
# Local
$ sql-migrate up
```

## Run Server

```
$ go run main.go
```

